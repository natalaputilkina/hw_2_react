import './ModalWrapper.scss'


function ModalWrapper ({children, onClose}){
    return <section className='modal-wrapper' onClick={onClose}>
        {children}        
    </section>

}
export default ModalWrapper