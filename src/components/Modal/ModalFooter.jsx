import "./ModalFooter.scss"
import Button from "../Button"



function ModalFooter({firstText, secondaryText, firstClick, secondaryClick}){
    return <div className="modal__footer">
        {firstText && <Button classNames='button_modal' onClick={firstClick}>{firstText}</Button>}
        {secondaryText && <Button classNames='button_modal2' onClick={secondaryClick}>{secondaryText}</Button>}
    </div>
}

export default ModalFooter


