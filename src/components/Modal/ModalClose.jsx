import "./ModalClose.scss"

function ModalClose ({onClick}){
    return <button className="modal-close" type="button" onClick={onClick}>X</button>
   
    
}

export default ModalClose