import "./ProductCard.scss"
import ModalPage from "../../pages/MainPage/ModalPage";
import IconStar from "../../assets/star.svg"
import starFavoritIcon from "../../assets/star-favorit.svg";


function ProductCard ({product, addToCart, addToFavorite, isFavorite}){

        const {id, name, imageUrl, price, color} = product    
    return (
        <div className="product">
            <h2 className="product--name">{name}</h2>
            <img className="product--img" src={imageUrl}></img> 
            <p className="product--price">Price {price} $</p>
            <p className="product--article">Article {id}</p>
            <div className="product--color">{color}</div>
            <ModalPage type = "first" title="Add to cart" click={() => addToCart(id)}/>
            <button className="favorite__button" onClick={() => addToFavorite(id)}>
                <img className="favorite--image" src={isFavorite ? starFavoritIcon : IconStar} alt="Favorite" />
            </button>
        </div>
    )
}


export default ProductCard