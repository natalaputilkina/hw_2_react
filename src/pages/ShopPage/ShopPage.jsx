import "./ShopPage.scss"
import ProductCard from "../../components/ProductCard/ProductCard"




function ShopPage({products, addToCart, addToFavorite, favorite}){

    

     return (   
        <div className="shop--page">
            {
             products.map(product => <ProductCard 
                                    addToCart = {addToCart}
                                    addToFavorite = {addToFavorite}
                                    product={product}
                                    key={product.id} 
                                    isFavorite={favorite.some(item => item.id === product.id)}
            
            />
            )}
        </div>           

    )
    

}
export default ShopPage