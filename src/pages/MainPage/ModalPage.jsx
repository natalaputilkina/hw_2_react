import { useState } from "react"
import Button from "../../components/Button"
import ModalText from "../../components/Modal/ModalText"
import ModalImage from "../../components/Modal/ModalImage"
import Modal from "../../components/Modal/Modal"
import ModalWrapper from "../../components/Modal/ModalWrapper"
import ModalHeader from "../../components/Modal/ModalHeader"
import ModalFooter from "../../components/Modal/ModalFooter"
import ModalClose from "../../components/Modal/ModalClose"
import ModalBody from "../../components/Modal/ModalBody"


function ModalPage({title, type, click}) {
  const [modalState, setModalState] = useState({
    isOpen: false,
    type: "",
  });

  const openModal = (type) => {
    setModalState({ isOpen: true, type });
  };

  const closeModal = () => {
    setModalState({ isOpen: false, type: "" });
  };
  
  return (
    <>
      <Button
        classNames="button_modal"
        title={title}
        onClick={() => openModal(type)}
      />

      {modalState.isOpen && (
        <ModalWrapper onClose={closeModal}>
          <Modal>
            {modalState.type === "first" && (
              <>
                <ModalHeader>
                  <ModalImage />
                </ModalHeader>
                <ModalClose onClick={closeModal} />
                <ModalBody>
                  <ModalText
                    title="Product add to cart!"
                    text="By clicking the “Yes, add” button, PRODUCT will be add to cart."
                  />
                </ModalBody>
                <ModalFooter
                  firstText="NO, CANCEL"
                  secondaryText="YES, ADD"
                  firstClick={closeModal}
                  secondaryClick={click}
                />
              </>
            )}
            {modalState.type === "second" && (
              <>
                <ModalClose onClick={closeModal} />
                <ModalBody>
                  <ModalText
                    title='Add Product “NAME”'
                    text="Description for your product"
                  />
                </ModalBody>
                <ModalFooter firstText="ADD TO FAVORITE" firstClick={closeModal} />
              </>
            )}
          </Modal>
        </ModalWrapper>
      )}
    </>
  );
}

export default ModalPage;