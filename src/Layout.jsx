import './Layout.scss'
import cartIcon from "../src/assets/cart.svg"
import starFavoritIcon from "../src/assets/star-favorit.svg"
// import starIcon from "../src/assets/star.svg"
import { useState, useEffect } from "react"
import ShopPage from "./pages/ShopPage/ShopPage"


function Layout(){

    const [cart, setCart] = useState(() => {
        const cart = JSON.parse(localStorage.getItem('cart')) || []
        return cart
    })
    
    const [favorite, setFavorite] = useState(() => {
        const favorite = JSON.parse(localStorage.getItem('favorite')) || []
        return favorite
    })
    
    const [products, setProducts] = useState([])
    

    useEffect(() => {
        fetch('products.json')
            .then (response => response.json())
            .then (data => setProducts (data))
    }, [])


    useEffect(() => {       
        localStorage.setItem("cart", JSON.stringify(cart));
    }, [cart]);
   

    useEffect(() => {       
        localStorage.setItem("favorite", JSON.stringify(favorite));
    }, [favorite]);



    const addToCart = (id) => {       
        const isProductInCart = cart.some(item => item.id === id);
        if (!isProductInCart) {
            const product = products.find(product => product.id === id);
            setCart([...cart, product]);
        } 

    };

    const addToFavorite = (id) => {       
        const isProductInFavorite = favorite.some(item => item.id === id);
        if (isProductInFavorite) {
            setFavorite(favorite.filter(item => item.id !== id));
        } else {
            const product = products.find(product => product.id === id);
            setFavorite([...favorite, product]);
        }

    };




    return (
        <>
        <header>
            <div className="header-shop">

                <h1 className="header-title">Mobile-shop</h1>   

                <div className='header-contener'>

                    <div className="header-favorites">
                        <img className='favorites-img' src={starFavoritIcon} alt="favorites"/>
                        <span className='favorites-count'>{favorite.length}</span>
                    </div>
                    <div className="header-cart">
                        <img className='cart-img' src={cartIcon} alt="cart"/>
                        <span className='cart-count'>{cart.length}</span>
                    </div>
                   
                    
                </div>     

            </div>

              
            
            
        </header>

        <ShopPage products = {products} addToCart = {addToCart} addToFavorite = {addToFavorite} favorite={favorite}/>
       

        
        </>
    )
}

export default Layout